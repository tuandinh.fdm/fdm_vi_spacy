# fdm_vi_spacy

#### For spacy version: 2.3

Init model with word2vec
 ~~~
python train_word_vecs.py
gzip output/vi_wordvectors.txt
python -m spacy init vi fdm_spacy_model --vectors-loc output/vi_wordvectors.txt.gz
 ~~~
Link to spacy
~~~
python -m spacy link fdm_spacy_model fdm_spacy_model
~~~

